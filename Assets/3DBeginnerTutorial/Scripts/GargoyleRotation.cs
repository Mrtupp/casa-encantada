using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GargoyleRotation : MonoBehaviour
{
    public float rotationSpeed = 10f;  // Velocidad de rotaci�n en grados por segundo
    public float rotationAngle = 90f;  // �ngulo de rotaci�n en grados

    private Quaternion initialRotation;

    void Start()
    {
        // Guarda la rotaci�n inicial
        initialRotation = transform.rotation;
    }

    void Update()
    {
        RotateGargoyle();
    }

    private void RotateGargoyle()
    {
        float angle = Mathf.PingPong(Time.time * rotationSpeed, rotationAngle);
        transform.rotation = initialRotation * Quaternion.Euler(0f, angle, 0f);
    }
}

